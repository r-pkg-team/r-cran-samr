Source: r-cran-samr
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-impute,
               r-cran-matrixstats,
               r-cran-shiny,
               r-cran-shinyfiles,
               r-cran-openxlsx,
               r-cran-gsa
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-samr
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-samr.git
Homepage: https://cran.r-project.org/package=samr
Rules-Requires-Root: no

Package: r-cran-samr
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R significance analysis of microarrays
 This GNU R package provides significance analysis of microarrays.
 A microarray is a multiplex lab-on-a-chip. It is a 2D array on a solid
 substrate (usually a glass slide or silicon thin-film cell) that assays
 large amounts of biological material using high-throughput screening
 miniaturized, multiplexed and parallel processing and detection methods.
 .
 This package helps analysing this kind of microarrays.
